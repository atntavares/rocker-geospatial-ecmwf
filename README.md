# rocker-geospatial-ecmwf

This image is based on [rocker/geospatial](https://hub.docker.com/r/rocker/geospatial/), a Docker-based Geospatial toolkit for R, built on versioned Rocker images. It adds support to the [ECMWF Web API Python client](https://software.ecmwf.int/wiki/display/CKB/How+to+download+ERA5+data+via+the+ECMWF+Web+API)

# Usage

Check out [this page](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image).

#  Running the ECMWF Python client to download data from R

One can download data from ECMWF Data Servers, by running a Python script from R. Be sure that the API key is available at `$HOME/.ecmwfapirc` with the following contents:

```
{
    "url"   : "https://api.ecmwf.int/v1",
    "key"   : "XXXXXXXXXXXXXXXXXXXXXX",
    "email" : "john.smith@example.com"
}

```
Assume we have the following `test.py` script:

```
#!/usr/bin/env python

from ecmwfapi import ECMWFDataServer
 
server = ECMWFDataServer()
server.retrieve({
    "class": "ea",
    "dataset": "era5",
    "expver": "1",
    "stream": "oper",
    "type": "an",
    "levtype": "sfc",
    "param": "165.128/166.128/167.128",
    "date": "2016-01-01/to/2016-01-02",
    "time": "00:00:00",
    "step": "0",
    "grid": "0.3/0.3",
    "target": "my_ERA5_test_file.grb"
 })

```

One can call it from R by running:

```
home <- "/set/home/directory/here"

setwd(home)
Sys.setenv(HOME = home)
system("python test.py")

```


